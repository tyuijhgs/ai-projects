import random
import argparse
import pygame
import numpy as np
import heapq
from pygame.locals import *
from collections import deque

DEBUG = False
DEBUG_AI = True

class Abstract_Heuristic:
    def __init__(self,model):
        self._model = model
    
    # dummy method for heuristic 
    def cal(self,state):
        return 1

# calculates the maximum possible occurance of a sequence in current state
class Count_Max(Abstract_Heuristic):

    def __init__(self,model):
        print("Generating Influence Map")

        board = np.zeros(model._board_size)
        good_point = np.array([board.shape[0]/2,board.shape[1]-1])
        max_d = np.linalg.norm(np.zeros(2) - np.array(board.shape).astype(np.float64))
        for x in range(board.shape[0]):
            for y in range(board.shape[1]):
                point = np.array([x,y])
                board[x,y] = max_d - np.linalg.norm(good_point-np.array([x,y]))
        self.heat_map = board.astype(np.int8)

        good_heat_map = np.array(
          [[2, 2, 2, 2, 2, 2, 2, 5],
           [2, 3, 3, 3, 3, 3, 3, 3],
           [2, 7, 7, 7, 7, 7, 7, 7],
           [2, 9, 9, 9, 9, 9, 9, 9],
           [2, 7, 7, 7, 7, 7, 7, 7],
           [2, 3, 3, 3, 3, 3, 3, 3],
           [2, 2, 2, 2, 2, 2, 2, 2]])

        self.heat_map = good_heat_map
        print(self.heat_map.astype(np.int8))
        super().__init__(model)
    

    def cal(self,state,player):
        b = state.get_board()
        player_1 = player
        player_2 = 1 if player == 2 else 2

        #player_1 = player
        #player_2 = self._model._next_player(state)

        p1_max = -float('inf')
        p2_max = -float('inf')
        p1_sum = 0
        p2_sum = 0

        

        for x in range(b.shape[0]):
            for y in range(b.shape[1]):
                if b[x,y] == player_1:
                    a = self._model._diagonal_main(b,(x,y))
                    e = self._model._diagonal_alt(b,(x,y))
                    c = self._model._horizontal(b,(x,y))
                    d = self._model._horizontal(np.transpose(b),(y,x))
                    p1_max = max(max(max(a,e),c),d)
                    p1_sum = p1_sum + self.heat_map[x,y]*(10**a + 10**e + 10**c + 10**d)
                if b[x,y] == player_2:
                    a = self._model._diagonal_main(b,(x,y))
                    e = self._model._diagonal_alt(b,(x,y))
                    c = self._model._horizontal(b,(x,y))
                    d = self._model._horizontal(np.transpose(b),(y,x))
                    p2_max = max(max(max(a,e),c),d)
                    p2_sum = p2_sum + self.heat_map[x,y]*(10**a + 10**e + 10**c + 10**d)

        r = 0
        if   p2_max >= self._model.win:
            r = -float('inf')
        elif p1_max >= self._model.win:
            r = float('inf')
        else :
            r = p1_sum-p2_sum

        return r
        
# new heuristic -> count number of sums in the middel -> every possible move must go through middle


class H_Threat(Abstract_Heuristic):

    def __init__(self,model):
        super().__init__(model)
    

    def cal(self,state,player):
        b = state.get_board()

        player_1 = player
        player_2 = 1 if player == 2 else 2

        n = self._model.win

        count = 0.0
        h_sum = 0.0

        p1_win = self._model._validate(state,[player_1]) >= n
        p2_win = self._model._validate(state,[player_2]) >= n

        for x in range(b.shape[0]):
            for y in range(b.shape[1]):
               
                p1c = [0,0,0,0]
                p2c = [0,0,0,0]
                pe = [False,False,False,False]

                for i in range(n):
                    try:
                        if b[x+i,y] == player_1:
                            p1c[0] += 1
                        elif b[x+i,y] == player_2:
                            p2c[0] += 1
                    except Exception:
                        pe[0] = True
                    try:
                        if b[x,y+i] == player_1:
                            p1c[1] += 1
                        elif b[x+i,y] == player_2:
                            p2c[1] += 1
                    except Exception:
                        pe[1] = True
                    try:
                        if b[x+i,y+i] == player_1:
                            p1c[2] += 1
                        elif b[x+i,y] == player_2:
                            p2c[2] += 1
                    except Exception:
                        pe[2] = True
                    try:
                        if b[x+i,y-i] == player_1:
                            p1c[3] += 1
                        elif b[x+i,y] == player_2:
                            p2c[3] += 1
                    except Exception:
                        pe[3] = True

                for i in range(len(p1c)):
                    # is not a counted line
                    if pe[i]:
                        pass

                    danger = min((n-1)/n,p2c[i]/n)
                    good   = min((n-1)/n,p1c[i]/n)
                    block  = 0

                    if p1c[i] > 0 and p2c[i] > 0:
                        block = 0.06
                    elif p1c[i] > 1 and p2c[i] > 1:
                        block = 0.11

                    if danger < 0.30:
                        danger = 0

                    count += 1

                    h_sum += good + block - danger

                    
        if p1_win and p2_win:
            return -100
        if p1_win:
            return 100
        if p2_win:
            return -200

        return h_sum/count


# does nothing
class Abstract_Strategy:
    def __init__(self,model,player_num,heuristic=None):
        self._model = model
        self._moves = None
        self._player = player_num
        self._heuristic = heuristic

    def get_next_move(self):
        pass

    # deprecated -- legacy
    def apply_next_move(self):
        pass


class Monte_Carlo(Abstract_Strategy):
    def get_next_move(self):
        moves = self._model.production_system(self._model._state)
        return random.choice(moves)
        
class Alpha_Beta(Abstract_Strategy):
    def __init__(self,model,player_num,max_depth=0,heuristic=None):
        self.max_depth = max_depth
        super().__init__(model,player_num,heuristic=heuristic)

    def get_next_move(self):
        print("AI -- Testing for Near Moves")
        
        for successor in self._model.production_system(self._model._state):
            if self._model._validate(successor,[self._player]) >= self._model.win:
                return successor  

        (s,v) = self.alpha_beta(self._model._state,0,-float('inf'),float('inf'),True)
        if DEBUG_AI:
            print("AI -- Best Possible Move : ",v)
        return s
    
    def alpha_beta(self,state,depth,a,b,maximizing_player): 
        if self.cutt_off(state,depth):
            return (state,self._heuristic.cal(state,self._player))
        
        if maximizing_player:
            value = -float('inf')
            s_r = None
            for successor in self._model.production_system(state):
                (s,val) = self.alpha_beta(successor,depth+1,a,b,False)

                value = max(val,value)
                a = max(a,value)

                if a >= b:
                    break
                if value == val:
                    s_r = successor

            return (s_r,value)
        else: # minimizing player
            value = float('inf')
            s_r = None
            for successor in self._model.production_system(state):
                (s,val) = self.alpha_beta(successor,depth+1,a,b,True)
                value = min(val,value)
                b = min(b,value)

                if a >= b:
                    break
                if value == val:
                    s_r = successor
            return (s_r,value)
    
    def cutt_off(self,state,depth):
        return depth > self.max_depth or self._model._validate(state,[1,2]) >=  self._model.win

class State:
    def __init__(self,board,player):
        self.state = (board,player)
    def get_board(self):
        return self.state[0]
    def get_player(self):
        return self.state[1]
    def __eq__(self,other):
        if isinstance(other,self.__class__):
            return np.array_equal(self.get_board(),other.get_board()) and self.get_player() == other.get_player()
        return False
    def __ne__(self,other):
        return not self.__eq__(other)
    def __str__(self):
        return str(self.state)
    def __repr__(self):
        return str(self.state)


class Model:
    def __init__(self):
        print("Creating Model")
        self.init((7,8),[False,False],[None,None],4,True)

    def init(self,board_size,ai_players,strategys,win,mode):
        print("Re-initializing Model")
        self._board_size = board_size
        self.ai_players = ai_players
        self.strategys = strategys
        self.win = win
        self.done = False
        self.move_history = []
        self.mode_toggle = mode
        self.reset()
    pass

    def reset(self):
        self._state = State(np.zeros(self._board_size,dtype=np.int8),1)
        self.counter = 0
        self.done = False

    def _validate(self,state,flag):
        b = state.get_board()
        m = 0
        for x in range(b.shape[0]):
            for y in range(b.shape[1]):
                if  b[x,y] in flag:
                    m = max(m,self._diagonal_main(b,(x,y)))
                    m = max(m,self._diagonal_alt(b,(x,y)))
                    m = max(m,self._horizontal(b,(x,y)))
                    m = max(m,self._horizontal(np.transpose(b),(y,x)))
        return m

    # find along the diagonal
    def _diagonal_alt(self,board,pos):
        counter = 0
        flag = board[pos]

        c_top_right = min(board.shape[0]-pos[0],pos[1]+1)
        c_bottom_left = min(pos[0]+1,board.shape[1]-pos[1])

        for i in range(c_top_right):
            if not board[pos[0]+i,pos[1]-i] == flag:
                break
            counter = counter +1

        for i in range(c_bottom_left):
            if not board[pos[0]-i,pos[1]+i] == flag:
                break
            counter = counter +1

        return counter -1
    
    # finds along the horizontal
    def _diagonal_main(self,board,pos):
        counter = 0
        flag = board[pos]

        c_top_left = min(board.shape[0]-pos[0],board.shape[1]-pos[1])
        c_bottom_right = min(pos[0]+1,pos[1]+1)

        for i in range(c_top_left):
            if not board[pos[0]+i,pos[1]+i] == flag:
                break
            counter = counter +1
        for i in range(c_bottom_right):
            if not board[pos[0]-i,pos[1]-i] == flag:
                break
            counter = counter +1

            
        return counter -1
    # return the number of tiles in a row
    def _horizontal(self,board,pos):
        counter = 0
        flag = board[pos]
        for x in range(pos[0],board.shape[0]):
            if not board[x,pos[1]] == flag:
                break
            counter = counter +1
        for x in range(pos[0]-1,-1,-1):
            if not board[x,pos[1]] == flag:
                break
            counter = counter + 1
        

        return counter

        

    def _next_player(self,state): 
        #next player turn
        player = state.get_player()
        if player == 1:
            return 2
        else:
            return 1

    # all the moves at this state
    def production_system(self,state):
        board    = state.get_board()
        player_b = state.get_player()
        player_a = self._next_player(state)
        states = []

        
        # add tiles to board
        for row in range(self._board_size[0]):
            for col in range(self._board_size[1]-1,-1,-1):
                if board[row][col] == 0:
                    temp = np.copy(board)
                    temp[row][col] = player_b
                    states.append(temp)
                    break
        
        if self.mode_toggle:

            # shift tiles down
            for row in range(self._board_size[0]):
                if board[row,self._board_size[1]-1] == player_b:
                    temp = np.copy(board)
                    for col in range(self._board_size[1]-1,-1,-1):
                        if not col == 0:
                            temp[row][col] = temp[row][col-1]
                    temp[row][0] = 0
                    states.append(temp)



        return [ State(state,player_a) for state in states]

    # mostly used for the player iteraction
    def produce_move(self,pos,state):
        production = self.production_system(state)
        board  = state.get_board()
        player_b = state.get_player()
        player_a = self._next_player(state)

        state_r = None

        # add tile to board
        if board[pos[0],pos[1]] == 0:
            # check if correct game state
            temp = np.copy(board)
            temp[pos[0],pos[1]] = player_b
            state_r = State(temp,player_a)

        #shift all tiles down
        elif board[pos[0],pos[1]] == player_b and pos[1] == self._board_size[1]-1:
            temp = np.copy(board)
            for col in range(self._board_size[1]-1,-1,-1):
                if not col == 0:
                    temp[pos[0]][col] = temp[pos[0]][col-1]
            temp[pos[0]][0] = 0
            state_r = State(temp,player_a)

        if state_r in production:
            return state_r
        return None

        
            
    # returns the board size currently being played on
    def get_board_size(self):
        return self._board_size

    # gets the next move defined by the srategy
    def player_move(self,pos):
        if self.done:
            return

        # its the AI's turn
        if self.ai_players[self.counter]:
            return
        move = self.produce_move(pos,self._state)
        if move is None:
            return

        self.next_state(move)

    def ai_move(self):
        if self.done:
            return
        if not self.ai_players[self.counter]:
            return 

        move = self.strategys[self.counter].get_next_move()
        if DEBUG_AI:
            print("AI Player {0} Move : ".format(self.counter+1))
            print(move)

        self.next_state(move)

    def next_state(self,state):
        self.move_history.append(state)
        self._state = state
        self._increment_player_counter()

    def get_state(self):
        return self._state

    # changes palyers and checks for winner, resets board if true
    def _increment_player_counter(self):
        self.counter = (self.counter + 1 ) % len(self.ai_players)

        if (self._validate(self._state,[1,2])>= self.win):
            self.done = True




class Controller:
    def __init__(self,args):

        self._browes_history = False
        self._history_index = 0

        self._model = Model()

        ai_names    = [args.p1,args.p2]
        ai_depth    = [args.d1,args.d2]
        ai_list     = [not name == 'player' for name in ai_names]
        ai_strategy = []
        mode = args.mode =='t'
        win  = args.w 
        size = (args.width,args.height)

        for i, name in enumerate(ai_names):
            model = self._model
            p = i +1
            print('player {0} is playing as : {1}'.format(p,name))
            if name == 'player':
                ai_strategy.append(None)
            if name == 'random':
                ai_strategy.append(Monte_Carlo(model,p,heuristic=None))
            if name == 'sig_lines':
                ai_strategy.append(Alpha_Beta(model,p,max_depth=ai_depth[i],heuristic=Count_Max(model)))
            if name == 'threats':
                ai_strategy.append(Alpha_Beta(model,p,max_depth=ai_depth[i],heuristic=H_Threat(model)))

        print('player types: ',ai_names) 
        print('player depth: ',ai_depth)
        print('Is AI       : ',ai_list)
        print('player strat: ',ai_strategy)
        print('mode        : ',mode)
        print('tiles 2 win : ',win)

        self._model.init(size,ai_list,ai_strategy,win,mode)




    def get_board(self):
        if self._browes_history and not len(self._model.move_history) < self._history_index+1:
            return np.copy(self._model.move_history[len(self._model.move_history)-self._history_index-1].get_board())
        return np.copy(self._model.get_state().get_board())


    # will have to be modified
    def next(self,pos):
        print('Player Move Call : {0}'.format(pos))
        self._model.player_move(pos)
        pass

    def ai_move(self):
        self._model.ai_move()

    def new_game(self):
        self._model.reset()

    def toggle_history(self):
        if self._browes_history:
            print("History OFF")
            self._browes_history = False
            self._history_index = 0
        else:
            print("History ON")
            self._browes_history = True

    def hist_inc(self):
        self._history_index+= 1
        print(self._history_index)

    def hist_dec(self):
        if self._history_index <= 0:
            return
        self._history_index+= -1
        print(self._history_index)
 
# templating code from : http://pygametutorials.wikidot.com/tutorials-two
class App:
    def __init__(self,args,speed=1000):
        self.pos = 0
        self._running = True
        self._display_surf = None
        self._image_surf = None
        self._bg_colour = (0,0,0)
        self._board = None
        self._x_boarder = 1000
        self._y_boarder = 1000
        self.strategy = "Null"
        self.speed = speed

        self._controller = Controller(args)

 
    def on_init(self):
        pygame.init()
        pygame.display.set_caption("4 Connect [{0}]".format(self.strategy))
        self._time = pygame.time.get_ticks()

        self._display_surf = pygame.display.set_mode((self._x_boarder,self._y_boarder), pygame.HWSURFACE)
        self._running = True


        # get this from controller class ( dummy test ) 
        self._board = self._controller.get_board()
 
    def on_event(self, event):
        if event.type == QUIT:
            self._running = False

    def on_loop(self):
        pass

    def on_render(self):
        self._board = self._controller.get_board()
        self._display_surf.fill(self._bg_colour)


        x_step = self._x_boarder/self._board.shape[0]
        y_step = self._y_boarder/self._board.shape[1]

        m_grid = self.get_mouse_pos()

        for row, x in zip(self._board,range(self._board.shape[0])):
            for val, y in zip(row,range(self._board.shape[1])):
                colour = (0,0,0)
                boarder = (255,255,255)
                margin  = 2

                # spider
                if val == 1:
                    colour = (255,0,0)
                # ant
                if val == 2:
                    colour = (0,255,0)

                if val == 3:
                    colour = (0,0,50)
                if val == 4:
                    colour = (100,0,50)
                if val == 5:
                    colour = (0,100,50)

                if m_grid == (x,y) and val == 0:
                    colour = (0,0,255)

                if m_grid == (x,y) and (val == 1 or val == 4):
                    colour = (150,0,100)

                if m_grid == (x,y) and  (val == 2 or val == 5):
                    colour = (0,150,100)

                pygame.draw.rect(self._display_surf,boarder,pygame.Rect((x_step*x,y_step*y,x_step,y_step)))
                pygame.draw.rect(self._display_surf,colour,pygame.Rect((x_step*x,y_step*y,x_step-margin,y_step-margin)))
            

        pygame.display.flip()

    def get_mouse_pos(self):
        pos     = pygame.mouse.get_pos()
        x_step = self._x_boarder/self._board.shape[0]
        y_step = self._y_boarder/self._board.shape[1]

        m_grid = (int((pos[0]/x_step)),int((pos[1]/y_step)))

        return m_grid


    def on_cleanup(self):
        pygame.quit()
 
    def on_execute(self):
        if self.on_init() == False:
            self._running = False
 
        pressed = False
        space = False
        left  = False
        right = False
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)

            keys = pygame.key.get_pressed()
            if keys[K_LEFT] == True and left == False:
                left = True

            if keys[K_LEFT] == False and left == True:
                left = False
                self._controller.hist_inc()

            if keys[K_RIGHT] == True and right == False:
                right =  True

            if keys[K_RIGHT] == False and right == True:
                right = False
                self._controller.hist_dec()

            if keys[K_r]:
                self._controller.new_game()

            if keys[K_SPACE] == True and space == False:
                space = True

            if keys[K_SPACE] == False and space == True:
                space = False
                self._controller.toggle_history()

            if pygame.mouse.get_pressed()[0] == True  and pressed == False:
                pressed = True

            if pygame.mouse.get_pressed()[0] == False and pressed == True:
                self._controller.next(self.get_mouse_pos())
                self._time = pygame.time.get_ticks()
                pressed = False

            if pygame.time.get_ticks() - self._time > self.speed:
                self._controller.ai_move()
                self._time = pygame.time.get_ticks()
            self.on_loop()
            self.on_render()
        self.on_cleanup()
 

if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description='---- 4 Connect ----')
    options_1 = ['player','random','sig_lines','threats']
    parser.add_argument('--p1', choices=options_1, help='Who is Player 1',default='player')
    parser.add_argument('--p2', choices=options_1, help='Who is Player 2',default='player')
    parser.add_argument('--d1',type=int, help='Depth of Search Player 1',default=3)
    parser.add_argument('--d2',type=int, help='Depth of Search Player 2',default=3)
    parser.add_argument('--w',type=int, help='Numer of Tiles to Win',default=4)
    parser.add_argument('--width',type=int, help='Width of Board (not recommened)',default=7)
    parser.add_argument('--height',type=int, help='Height of Board (not recommened)',default=8)
    parser.add_argument('--mode',type=str, help='t : 4 Connect, f : Connect 4',default='t')


    
    parser.add_argument('--speed',type=int, help='frames period time (ms)',default=0)

    args = parser.parse_args()
    app = App(args,speed=args.speed)


    app.on_execute()
