import random
import argparse
import pygame
import numpy as np
import heapq
from pygame.locals import *
from collections import deque

# heuristic
def manhatten(state):
    spider = GameState.spider(state)
    ant    = GameState.ant(state)
    # manhatten Distance
    dist = abs(spider[0] -ant[0]) + abs(spider[1] - ant[1])
    return dist

def euclidian(state):
    spider = GameState.spider(state)
    ant    = GameState.ant(state)
    # manhatten Distance
    dist = abs(((spider[0] -ant[0])**2 + ((spider[1] - ant[1])**2)**(0.5)))
    return dist

# take euclidian distance
# take ant velocity
# predict ant position based on agent position and velocity
# return new distance based on prediction
def moves_ahead(state):
    dist_0 = euclidian(state)
    dist_1 = int(dist_0/2.0)
    spider = GameState.spider(state)
    ant    = GameState.ant(state)
    vel    = GameState.vel(state)
    new_ant_pos = ((ant[0]+vel[0]*dist_1),(ant[1]+vel[1]*dist_1))
    state_0 = GameState.update_ant(new_ant_pos,state)
    dist_2 = euclidian(state_0)
    return dist_2

def combo(state):
    return (moves_ahead(state) + euclidian(state))/2.0

# does nothing
class Abstract_Strategy:
    def __init__(self,model,heuristic=None):
        self._model = model
        self._moves = None

    def get_next_move(self):
        return self._model._state

    def apply_next_move(self):
        self._model._turn(GameState.spider(self._model._state))


class Strategy_BFS(Abstract_Strategy):
    def __init__(self,model,heuristic=None):
        print("BREATH FIRST SEARCH INIT")
        Abstract_Strategy.__init__(self,model)
        self._moves = deque()

    def get_next_move(self):
        if len(self._moves) > 0:
            return self._moves[0]
        else: 
            return GameState.spider(self._model._state)

    def apply_next_move(self):
        # search for moves
        if len(self._moves) < 1:
            self._moves = self._search()
        
        if len(self._moves) > 0:
            print("Deterministic Move")
            #print(self._moves)
            self._model._turn(GameState.spider(self._moves.popleft()))
        else:
            print("Random Move")
            self._model._turn(random.choice(self._model.get_spider_moves(self._model._state)))


    # BFS search
    def _search(self):
        state = self._model._state
        queue = deque()
        dikes = dict()

        queue.extend(Move.convert_moves(None,self._model.get_spider_moves(state),state,self._model))

        final_move= None
        
        while len(queue) > 0:
            move = queue.popleft()

            if  not dikes.get(move.get_state(),None) is None:
                continue 
            dikes[move.get_state()] = True

            if GameState.ant(move.get_state()) == GameState.spider(move.get_state()):
                final_move = move
                break

            moves = Move.convert_moves(move,self._model.get_spider_moves(move.get_state()),move.get_state(),self._model)
            queue.extend(moves)

        moves  = []
        curr = final_move
        while not curr is None:
            moves.insert(0,curr.get_state())
            curr = curr.get_parent()


        return deque(moves)
        

class Strategy_DFS(Abstract_Strategy):
    def __init__(self,model,heuristic=None):
        print("DEPTH FIRST SEARCH INIT")
        Abstract_Strategy.__init__(self,model)
        self._moves = deque()

    def get_next_move(self):
        if len(self._moves) > 0:
            return self._moves[0]
        else: 
            return GameState.spider(self._model._state)

    def apply_next_move(self):
        # search for moves
        if len(self._moves) < 1:
            self._moves = self._search()
        
        if len(self._moves) > 0:
            print("Deterministic Move")
            #print(self._moves)
            self._model._turn(GameState.spider(self._moves.popleft()))
        else:
            print("Random Move")
            self._model._turn(random.choice(self._model.get_spider_moves(self._model._state)))


    # BFS search
    def _search(self):
        state = self._model._state
        queue = deque()
        dikes = dict()

        queue.extendleft(Move.convert_moves(None,self._model.get_spider_moves(state),state,self._model))

        final_move= None
        
        while len(queue) > 0:
            move = queue.popleft()

            if  not dikes.get(move.get_state(),None) is None:
                continue 
            dikes[move.get_state()] = True

            if GameState.ant(move.get_state()) == GameState.spider(move.get_state()):
                final_move = move
                break

            moves = Move.convert_moves(move,self._model.get_spider_moves(move.get_state()),move.get_state(),self._model)
            queue.extendleft(moves)

        moves  = []
        curr = final_move
        while not curr is None:
            moves.insert(0,curr.get_state())
            curr = curr.get_parent()


        return deque(moves)




class A_Star(Abstract_Strategy):
    def __init__(self,model,heuristic=None):
        print("A* Init")
        Abstract_Strategy.__init__(self,model)
        self._h = heuristic
        self._moves = deque()

    def get_next_move(self):
        if len(self._moves) > 0:
            return self._moves[0]
        else: 
            return GameState.spider(self._model._state)

    def apply_next_move(self):
        # search for moves
        if len(self._moves) < 1:
            self._moves = self._search()
        
        if len(self._moves) > 0:
            print("Deterministic Move")
            #print(self._moves)
            self._model._turn(GameState.spider(self._moves.popleft()))
        else:
            print("Random Move")
            self._model._turn(random.choice(self._model.get_spider_moves(self._model._state)))


    # A* search
    def _search(self):
        start_state = self._model._state
        
        # priority queue
        queue = []
        
        # already evaluated Nodes 
        closed_set = set()

        # current discoved nodes
        open_set = {start_state}

        #location of where nodes came from
        came_from = {}

        # cost to get to this state
        g_score = {}

        # cost from start-start is nothing
        g_score[start_state] = 0

        f_score = {}

        # cost from this state to goal
        f_score[start_state] = self._h(start_state)

        heapq.heappush(queue,(f_score[start_state],start_state))

        final_state = None

        # search 
        while len(open_set) > 0:
            p, current = heapq.heappop(queue)

            # reached goal
            if GameState.ant(current) == GameState.spider(current):
                final_state = current
                break

            # remove from open set add to closed set
            open_set.remove(current)
            closed_set.add(current)
            
            # call the production system to simulate a turn for next moves
            neigbours = [move.get_state() for move in Move.convert_moves(None,self._model.get_spider_moves(current),current,self._model)]


            for neigbour in  neigbours:
                # see this already
                if neigbour in closed_set:
                    continue

                # atcual path distance to get to node
                tentitive_g_score = g_score[current] + 1
                tentitive_f_score = tentitive_g_score + self._h(neigbour)
                
                # net move in open set
                if not neigbour in open_set:
                    open_set.add(neigbour)
                    heapq.heappush(queue,(tentitive_f_score,neigbour))
                # there exists a better move
                elif tentitive_g_score >= g_score[neigbour]:
                    continue

                came_from[neigbour] = current
                g_score[neigbour] = tentitive_g_score
                f_score[neigbour] = tentitive_f_score



       
        moves = []
        curr = final_state
        while not curr is None:
            moves.insert(0,curr)
            curr = came_from.get(curr,None)

        # remove inital state move
        if len(moves) > 0:
            moves = moves[1::]

        return deque(moves)






class GameState:
    def spider(state):
        return state[0]
    def ant(state):
        return state[1]
    def vel(state):
        return state[2]
    def new(spider,ant,vel):
        return (spider,ant,vel)
    def update_spider(spider,state):
        return (spider,state[1],state[2])
    def update_ant(ant,state):
        return (state[0],ant,state[2])
    def update_vel(vel,state):
        return (state[0],state[1],vel)
    def new_ant(ant,vel,state):
        return (state[0],ant,vel)

class Move:
    def __init__(self,state,parent=None):
        self._parent = parent
        self._state  = state

    def get_state(self):
        return self._state

    def get_parent(self):
        return self._parent

    def convert_moves(parent,moves,state,model):
        m = []
        for move in moves:
            s = model._simulate_turn(move,state)
            if not s is None:
                m.append(Move(s,parent))
        return m

    def __str__(self):
        return str(self._parent)+"->"+str(self._state)





class Model:
    def __init__(self,board_size=(5,5),strategy=Strategy_DFS,heuristic=None,generate_ant=False,gen_zero=False):
        self._board_size = board_size
        self._state = GameState.new((2,1),(1,2),(0,1))
        self._generate_new_ant = generate_ant
        self._gen_zero = gen_zero
        self._strategy = strategy(self,heuristic=heuristic)

    def _generate_ant(self):
        pos = (random.randint(0,self._board_size[0]-1),random.randint(0,self._board_size[1]-1))
        dire= (random.randint(-1,1),random.randint(-1,1))

        while dire == (0,0) and not self._gen_zero:
            dire= (random.randint(-1,1),random.randint(-1,1))

        print("New Ant Generate : at {0} in {1}".format(pos,dire))
        self._state = GameState.new_ant(pos,dire,self._state)


    # input : list of 2d tuples representing move actions
    # output: list of 2d tuples representing legit moves
    def _filter_correct_moves(self,moves):
        return [ (x,y) for x,y in moves if x>= 0 and x < self._board_size[0] and y >= 0 and y < self._board_size[1]]       

    # get possible spider moves on the board
    def get_spider_moves(self,state):
        x,y = GameState.spider(state)
        moves = [(x-1,y),(x-1,y-1),(x,y-1),(x+1,y-1),(x+1,y),(x-2,y+1),(x-1,y+2),(x+1,y+2),(x+2,y+1)]
        return self._filter_correct_moves(moves)

    # get the possible ant moves
    def get_ant_moves(self,state):
        x,y = GameState.ant(state)
        xv,yv = GameState.vel(state) 

        return self._filter_correct_moves([(x+xv,y+yv)])

    # returns the board size currently being played on
    def get_board_size(self):
        return self._board_size


    # to be called from strategy, applies its move 
    def _turn(self,spider_move):
        ant_m = self.get_ant_moves(self._state)

        self._state = GameState.update_spider(spider_move,self._state)

        if len(ant_m) > 0:
            self._state = GameState.update_ant(ant_m[0],self._state)

        if spider_move == GameState.ant(self._state):
            print("Ant Killed")
            self._generate_ant()
            pass
        elif len(ant_m) < 1 and self._generate_new_ant:
            print("Ant Off Screen")
            self._generate_ant()
            pass

    # called when want to simulate a turn
    def _simulate_turn(self,spider_move,state):
        ant_m = self.get_ant_moves(state)
        s = state


        s = GameState.update_spider(spider_move,s)
        # ant leaves area
        if len(ant_m) < 1 and not self._generate_new_ant:
            s = GameState.update_vel((0,0),s)
        elif len(ant_m) < 1 and self._generate_new_ant:
            return None
        else:
            s = GameState.update_ant(ant_m[0],s)

        return s




    # gets the next move defined by the srategy
    def next_move(self):
        self._strategy.apply_next_move()

    def get_state(self):
        return self._state



class Contoller:
    def __init__(self,board_size=(50,50),strategy='breath',heuristic='manhatten',death='zero',gen_zero='f'):

        # decide strategy
        if strategy == 'breath':
            self.strategy = Strategy_BFS
        if strategy == 'depth':
            self.strategy = Strategy_DFS
        if strategy == 'a-star':
            self.strategy = A_Star 

        # decide heuristic
        if heuristic == 'manhatten':
            self.heuristic = manhatten
        if heuristic == 'prediction':
            self.heuristic = moves_ahead
        if heuristic == 'average':
            self.heuristic = combo
        if heuristic == 'euclidian':
            self.heuristic = euclidian

        if death == 'zero':
            self.death = False
        if death == 'kill':
            self.death = True

        if gen_zero == 'f':
            self.gen_zero = False
        if gen_zero == 't':
            self.gen_zero = True

        self._model = Model(board_size,strategy=self.strategy,heuristic=self.heuristic,generate_ant=self.death,gen_zero=self.gen_zero)

    def get_board(self):
        board = np.zeros(self._model.get_board_size(),dtype=np.int8)
        state = self._model.get_state()

        # draw spider moves
        spider_m = self._model.get_spider_moves(state)
        for move in spider_m:
            board[move] = 3

        # draw ant moves
        ant_m = self._model.get_ant_moves(state)
        for move in ant_m:
            board[move] = 4

        #draw spider and ant
        board[GameState.ant(state)] = 2

        board[GameState.spider(state)] = 1
        
        board = np.flip(np.transpose(board),axis=0)
        return board

    def next(self):
        self._model.next_move()
 
# templating code from : http://pygametutorials.wikidot.com/tutorials-two
class App:
    def __init__(self,strategy='breath',heuristic='manhatten',death='zero',gen_zero='f',speed=0):
        self.pos = 0
        self._running = True
        self._display_surf = None
        self._image_surf = None
        self._bg_colour = (0,0,0)
        self._board = None
        self._x_boarder = 1000
        self._y_boarder = 1000
        self.strategy = strategy
        self.heuristic = heuristic
        self.death = death
        self.gen_zero = gen_zero
        self.speed = speed

        self._controller = Contoller(strategy=self.strategy,heuristic=self.heuristic,death=self.death,gen_zero=self.gen_zero)

 
    def on_init(self):
        pygame.init()
        pygame.display.set_caption("Spider Game Running [{0}]".format(self.strategy))
        self._time = pygame.time.get_ticks()

        self._display_surf = pygame.display.set_mode((self._x_boarder,self._y_boarder), pygame.HWSURFACE)
        self._running = True
        self._image_surf = pygame.image.load("myimage.jpg").convert()


        # get this from controller class ( dummy test ) 
        self._board = self._controller.get_board()
 
    def on_event(self, event):
        if event.type == QUIT:
            self._running = False

    def on_loop(self):
        pass

    def on_render(self):
        self._board = self._controller.get_board()
        self._display_surf.fill(self._bg_colour)

        x_step = self._x_boarder/self._board.shape[0]
        y_step = self._y_boarder/self._board.shape[1]

        for row, y in zip(self._board,range(self._board.shape[0])):
            for val, x in zip(row,range(row.shape[0])):
                colour = (0,0,0)
                boarder = (255,255,255)
                margin  = 2

                # spider
                if val == 1:
                    colour = (255,0,0)
                # ant
                if val == 2:
                    colour = (0,255,0)
                # colour of moves of spider
                if val == 3:
                    colour = (0,125,150)
                # colour of ant move
                if val == 4:
                    colour = (0,125,150)
                    

                pygame.draw.rect(self._display_surf,boarder,pygame.Rect((x_step*x,y_step*y,x_step,y_step)))
                pygame.draw.rect(self._display_surf,colour,pygame.Rect((x_step*x,y_step*y,x_step-margin,y_step-margin)))
            

        pygame.display.flip()
 
    def on_cleanup(self):
        pygame.quit()
 
    def on_execute(self):
        if self.on_init() == False:
            self._running = False
 
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            if pygame.time.get_ticks() - self._time > self.speed:
                print("Next Turn")
                self._controller.next()
                self._time = pygame.time.get_ticks()
            self.on_loop()
            self.on_render()
        self.on_cleanup()
 
if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description='Spider Game')
    options_1 = ['breath','depth','a-star']
    parser.add_argument('--search', choices=options_1, help='Search Algorithms',default='breath')

    options_2 = ['manhatten','prediction','average','euclidian']
    parser.add_argument('--heuristic', choices=options_2, help='Heuristic Algorithms',default='manhatten')
    
    options_3 = ['kill','zero']
    parser.add_argument('--death', choices=options_3, help='What happens when ant hits wall',default='zero')

    options_4 = ['t','f']
    parser.add_argument('--vel', choices=options_4, help='Is Velocity Zero',default='f')

    parser.add_argument('--speed',type=int, help='frames period time (ms)',default=0)

    args = parser.parse_args()
    

    theApp = App(strategy=args.search,heuristic=args.heuristic,death=args.death,gen_zero=args.vel,speed=args.speed)
    theApp.on_execute()
