
from DecisionTree import DecisionTree
from Bayes import NaiveBayesGaussian
from Bayes import NaiveBayesBinary 
from Bayes import BayesBinary

from utils import Generator

import pandas as pd
import numpy as np
from sklearn.model_selection import KFold 
import json


def X_to_binary(X,v_thres):
    Z = np.copy(X)
    Q = X[::,0:X.shape[1]-1]

    for i in range(Q.shape[0]):
        for j in range(Q.shape[1]):
            if Z[i,j] < v_thres[j]:
                Z[i,j] = 0.0
            else:
                Z[i,j] = 1.0
    return Z

def random_threshold(X):
    v_max = np.asarray([ np.max(X[::,i]) for i in range(X.shape[1]-1)])
    v_thres = v_max*np.random.rand(X.shape[1]-1)
    return v_thres

def find_good_threshold(X,random_pool=10000):
    curr_acc = -float('inf')
    curr_threshold = None

    for i in range(random_pool):
        temp_model = NaiveBayesBinary()
        threshold = random_threshold(X)
        temp_X = X_to_binary(X,threshold)

        temp_model.train(temp_X)

        s = 0.0
        for i in range(temp_X.shape[0]):
            l = temp_model.classify(temp_X[i,0:temp_X.shape[1]-1]) 
            if int(l[0]) == int(temp_X[i,temp_X.shape[1]-1]):
                s += 1.0
        acc = s/temp_X.shape[0]

        if acc > curr_acc:
            curr_acc = acc
            curr_threshold = threshold

    return curr_acc,curr_threshold


if __name__ == "__main__" :
    np.random.seed(0)

    decision_tree = DecisionTree()
    naive_bayes_gaussian = NaiveBayesGaussian()
    naive_bayes_Binary = NaiveBayesBinary()
    bayes_binary = BayesBinary()

    names = ['decision_tree','naive_bayes_gaussian','naive_bayes_Binary']
    classifiers = [decision_tree,naive_bayes_gaussian,naive_bayes_Binary]
    data_type = [0,1,1]
    accuracies = [[] for i in names]
    mean = []
    std  = []
    var  = []
    data = {}
    

    X = pd.read_csv("glass.data",header=None).values[::,1:11]
    X = np.asarray(X)

    

    for i in range(X.shape[0]):
        if X[i,9]<= 4.0:
            X[i,9] = 0.0
        else:
            X[i,9] = 1.0


    print("finding a GOOD threshold")
    nada , threshold = find_good_threshold(X,random_pool=1000)
    print("threshold acc: {0}".format(nada))
    print(threshold)

    B = X_to_binary(X,threshold)


    kf = KFold(n_splits=5, random_state=0, shuffle=True)

    counter = 0

    for train_index,test_index in  kf.split(X):
        counter += 1
        print('Starting Traiing On Kfold {0}'.format(counter))
        x_train = np.take(X,train_index,axis=0)
        y_test = np.take(X,test_index,axis=0)

        x_train_b = np.take(B,train_index,axis=0)
        y_test_b = np.take(B,test_index,axis=0)

        x_trains = [x_train,x_train_b]
        y_tests  = [y_test, y_test_b]

        for i in range(len(names)):
            print("Training {0} on {1} samples".format(names[i],len(x_train)))
            classifiers[i].train(x_trains[data_type[i]])

        for i in range(len(names)):
            s = 0.0
            acc = 0.0
            for j in range(len(test_index)):
                l = classifiers[i].classify(y_tests[data_type[i]][j,0:y_test.shape[1]-1])
                if int(y_tests[data_type[i]][j,y_test.shape[1]-1]) == int(l[0]):
                    s += 1
            acc =  s/len(y_test)
            accuracies[i].append(acc)
            print("Testing accuracie : {0} on {1}".format(acc,names[i]))

    accuracies = np.asarray(accuracies)
    
    for i in range(len(names)):
        mean.append(np.mean(accuracies[i]))
        std.append(np.std(accuracies[i]))
        var.append(np.var(accuracies[i]))

    print(mean)
    print(std)
    print(var)

    for i in range(len(names)):
        m = {} 
        m['name'] = names[i]
        m['mean'] = mean[i]
        m['std']  = std[i]
        m['var']  = var[i]
        data[names[i]] = m


    results = json.dumps(data,sort_keys=True,indent=4,separators=(',',': '))
    print(results)

    with open('data_2.txt','w') as f:
        f.write(results)

    classifiers[0].graph()
