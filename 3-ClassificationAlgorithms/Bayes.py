import math
import numpy as np
from utils import Generator
from utils import create_model
from pprint import pprint


class Bayes():
    def __init__(self):
        pass

    def train(self,X):
        pass

    def classify(self,x):
        pass

class NaiveBayesGaussian(Bayes):
    def train(self,X):
        self.classes = list(set(X[::,X.shape[1]-1].astype('int')))
        self.p_classes = np.zeros(len(self.classes))
        self.cov_m = []
        self.cov_m_inv = []
        self.mean  = []
        self.det   = np.zeros(len(self.classes))


        # seperate the training data
        indicies = [[] for i in self.classes]
        for i in range(len(X)):
            indicies[int(X[i,X.shape[1]-1])].append(i)

        training = [np.take(X,index,axis=0)[::,0:X.shape[1]-1] for index in indicies]


        # pre-calculations
        for c in self.classes: 
            w = training[c]
            self.p_classes[c] = len(w)/len(X)
            self.mean.append(np.mean(w,axis=0))
            self.cov_m.append(np.cov(w.T))
            self.cov_m_inv.append(np.linalg.inv(self.cov_m[c]))
            self.det[c] = np.linalg.det(self.cov_m[c])

        self.cov_m_inv = np.asarray(self.cov_m_inv)
        self.cov_m = np.asarray(self.cov_m)
        self.mean = np.asarray(self.mean)


    def classify(self,x):
        curr_v = -float('inf')
        curr_c = None
        s = 0.0

        for c in self.classes:
            p_c = self.p_classes[c]
            inv = self.cov_m_inv[c]
            mean = self.mean[c]
            det = self.det[c]

            p = math.pow(math.pow((2*np.pi),len(x))*det,-0.5)*math.pow(np.e,(-(x-mean).T@inv@(x-mean)/2))
            
            s += p

            if p > curr_v:
                curr_v = p
                curr_c = c


        probility = curr_v/s
        error = 1.0-probility

        return (curr_c,probility,error)

class NaiveBayesBinary(Bayes):

    def train(self,X):
        self.classes = list(set(X[::,X.shape[1]-1].astype('int')))
        self.p_one = np.zeros((len(self.classes),X.shape[1]-1))
        self.p_zero= np.zeros((len(self.classes),X.shape[1]-1))
        self.p_classes = np.zeros(len(self.classes))

        # seperate the training data
        indicies = [[] for i in self.classes]
        for i in range(len(X)):
            indicies[int(X[i,X.shape[1]-1])].append(i)

        training = [np.take(X,index,axis=0) for index in indicies]
        
        # calculate probilites
        for c in self.classes: 
            w = training[c]

            self.p_classes[c] = len(w)/len(X)
            for i in range(w.shape[1]-1):
                p_one = sum((w[::,i])/(w.shape[0]))
                self.p_one[c,i] = (p_one*w.shape[0]+1)/(w.shape[0]+2)
                self.p_zero[c,i]= ((1-p_one)*w.shape[0]+1)/(w.shape[0]+2)

    def classify(self,x):
        curr_v = -float('inf')
        curr_c = None
        s = 0.0

        for c in self.classes:

            p = self.p_classes[c]
            for i , v in enumerate(x):
                p1 = self.p_one[c,i]
                p0 = self.p_zero[c,i]
                p = p*(math.pow(p1,v))*(math.pow(p0,1.0-v))

            s += p

            if p > curr_v:
                curr_v = p
                curr_c = c


        probility = curr_v/s
        error = 1.0-probility

        return curr_c, probility, error


class BayesBinary(Bayes):

    def train(self,X):
        self.tree, self.classes = create_model(X)

    def classify(self,x):
        curr_c = None
        curr_v = -float('inf')
        s = 0.0
        for w,c in zip(self.tree,self.classes):
            p = self._classify(x,w)
            s += p
            if p > curr_v:
                curr_v = p
                curr_c = c
            #print(c,p,curr_v)
        return (curr_c,curr_v/s)
            

        

    def _classify(self,x,node):
        p = 0.0
        # choose probilites

        # root node
        if node.root is None:
            # feature x is zero
            if x[node.feature] == 0.0:
                p = node.class_p0
            # feature x is one
            else:
                p = node.class_p1
        # not root node
        else:
            # given that xr = 0, and xi = 0
            if x[node.feature] == 0.0 and x[node.root.feature] == 0.0 :
                p = node.pxi0_pxr0
            # given that xr = 1, and xi = 0
            if x[node.feature] == 1.0 and x[node.root.feature] == 0.0 :
                p = node.pxi1_pxr0
            # given that xr = 0, and xi = 1
            if x[node.feature] == 0.0 and x[node.root.feature] == 1.0 :
                p = node.pxi0_pxr1
            # given that xr = 1, and xi = 1
            if x[node.feature] == 1.0 and x[node.root.feature] == 1.0 :
                p = node.pxi1_pxr1

        if node.children is None:
            return p

        for child in node.children:
            p = p*self._classify(x,child)

        return p



    def print(self):
        for item ,w in zip(self.tree,self.classes):
            print("#Class : ", w)
            print(item)
        pass



if __name__ == "__main__" :
    n = NaiveBayesBinary()
    ng = NaiveBayesGaussian()
    b = BayesBinary() 

    print("Generating Data")
    g = Generator(classes=4,dim=10, samples_per_class=500)
    i = g.generate()

    print("Training Model Naive Bayes Binary Distribution")
    n.train(i)
    print("Training Model Naive Bayes Guassian Distribution")
    ng.train(i)
    print("Training Model Bayes")
    b.train(i)
    b.print()

    x = i[1,::]

    s = 0
    for x in i:
        l = n.classify(x[0:10])
        if l[0] == int(x[10]):
            s += 1.0

    print("Naive Bayes Bernoulli classification")
    print(s/len(i))

    s = 0
    for x in i:
        l = ng.classify(x[0:10])
        if l[0] == int(x[10]):
            s += 1.0

    print("Naive Bayes Gaussian classification")
    print(s/len(i))

    s = 0
    for x in i:
        l = b.classify(x[0:10])
        if l[0] == int(x[10]):
            s += 1.0

    print("Bayes Binary classification")
    print(s/len(i))
