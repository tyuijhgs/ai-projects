from DecisionTree import DecisionTree
from Bayes import NaiveBayesGaussian
from Bayes import NaiveBayesBinary 
from Bayes import BayesBinary
from sklearn.model_selection import KFold 
from utils import Generator
import numpy as np
import json

if __name__ == "__main__" :
    decision_tree = DecisionTree()
    naive_bayes_gaussian = NaiveBayesGaussian()
    naive_bayes_Binary = NaiveBayesBinary()
    bayes_binary = BayesBinary()

    names = ['decision_tree','naive_bayes_gaussian','naive_bayes_Binary','bayes_binary']
    classifiers = [decision_tree,naive_bayes_gaussian,naive_bayes_Binary,bayes_binary]
    accuracies = [[] for i in names]
    mean = []
    std  = []
    var  = []
    data = {}
    

    g = Generator(classes=4,dim=10, samples_per_class=500)
    X = g.generate()
    kf = KFold(n_splits=5, random_state=0, shuffle=True)

    counter = 0

    for train_index,test_index in  kf.split(X):
        counter += 1
        print('Starting Traiing On Kfold {0}'.format(counter))
        x_train = np.take(X,train_index,axis=0)
        y_test = np.take(X,test_index,axis=0)

        for i in range(len(names)):
            print("Training {0} on {1} samples".format(names[i],len(x_train)))
            classifiers[i].train(x_train)

        for i in range(len(names)):
            s = 0.0
            acc = 0.0
            for j in range(len(test_index)):
                l = classifiers[i].classify(y_test[j,0:y_test.shape[1]-1])
                if int(y_test[j,y_test.shape[1]-1]) == int(l[0]):
                    s += 1
            acc =  s/len(y_test)
            accuracies[i].append(acc)
            print("Testing accuracie : {0} on {1}".format(acc,names[i]))

    accuracies = np.asarray(accuracies)
    
    for i in range(len(names)):
        mean.append(np.mean(accuracies[i]))
        std.append(np.std(accuracies[i]))
        var.append(np.var(accuracies[i]))

    print(mean)
    print(std)
    print(var)

    for i in range(len(names)):
        m = {} 
        m['name'] = names[i]
        m['mean'] = mean[i]
        m['std']  = std[i]
        m['var']  = var[i]
        data[names[i]] = m


    results = json.dumps(data,sort_keys=True,indent=4,separators=(',',': '))
    print(results)

    with open('data_1.txt','w') as f:
        f.write(results)
           

    


            



