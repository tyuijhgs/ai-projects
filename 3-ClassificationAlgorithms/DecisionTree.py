# Christopher Blackman

import math
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from utils import Generator
from networkx.drawing.nx_agraph import write_dot, graphviz_layout

# entropy takes list of the number of clases [X,Y,Z...]
def entropy(c):
    total = sum(c)
    running_s = 0
    for ci in c:
        if ci <= 0:
            continue
        running_s += -(ci/total)*math.log(ci/total,2)
    return running_s

# infomation gain from a certain feature
def infomation_gain(original,split_1,split_2):
    o = count_classes(original)
    s1 = count_classes(split_1)
    s2 = count_classes(split_2)

    gain = entropy(o) - (sum(s1)/sum(o))*entropy(s1)-(sum(s2)/sum(o))*entropy(s2) 
    return gain

# splits data based on threshold
def partition(features,question):
    true  = []
    false = []

    for i in range(len(features)):
        if question.match(features[i]):
            true.append(i)
        else:
            false.append(i)

    features_true  = np.take(features,true,axis=0)
    features_false = np.take(features,false,axis=0)

    return (features_true,features_false)

# counts the classes and returns a number of clases of form [X,Y,Z,..]
def count_classes(features,as_list=True):
    classes = unique_vals(features[::,features.shape[1]-1])
    for item in features:
        classes[item[-1]] += 1
    if as_list:
        return [item for key,item in classes.items()]
    return classes

# finds all unique elements in a coloumn and return a map mapping to zero
def unique_vals(x):
    temp = set(x)
    return {i:0 for i in x}



# code below used the following code as a reference :
# https://github.com/random-forests/tutorials/blob/master/decision_tree.py

def find_best_split(features):
    curr_gain = 0
    curr_q    = None
    n_features = features.shape[1]-1

    for feature_index in range(n_features):
        values = set(features[::,feature_index])
        for val in values:
            question = Question(feature_index,val)
            features_true, features_false = partition(features,question)
            gain = infomation_gain(features,features_true,features_false)

            if gain > curr_gain:
                curr_gain = gain
                curr_q = question

    return curr_gain, curr_q


def build_tree(features):
    gain, question = find_best_split(features) 

    if gain == 0:
        return Leaf_Node(features)

    features_true, features_false = partition(features,question)

    if_true  = build_tree(features_true)
    if_false = build_tree(features_false)

    return Decision_Node(question,if_true,if_false)

# decision at a node
class Question():
    def __init__(self,feature_index,threshold=0.5):
        self.feature_index = feature_index
        self.threshold = threshold 

    def match(self,example):
        val = example[self.feature_index]
        return val <= self.threshold

    def __repr__(self):
        return "x_{0}<={1}".format(self.feature_index,self.threshold)

class Leaf_Node():
    '''
        holds a dictinary of predictions based on training data
    '''
    def __init__(self,features):
        self.predictions = count_classes(features,as_list=False)


class Decision_Node():
    '''
    used a a refence to a question
    '''
    def __init__(self,question,true_branch,false_branch):
        self.question = question
        self.true_branch = true_branch
        self.false_branch = false_branch

def print_tree(node,spacing="",depth=0,max_depth=float('inf')):
    if depth > max_depth:
        return

    if isinstance(node,Leaf_Node):
        print(spacing + str(node.predictions))
        return
    print(spacing+str(node.question))
    print(spacing+"True -->")
    print_tree(node.true_branch,spacing + "    ",depth +1,max_depth)
    print(spacing+"False-->")
    print_tree(node.false_branch,spacing + "    ",depth+1,max_depth)

def graph_tree(node,i,depth=0,m_depth=3,graph=None,addition=""):
    #root node
    if graph == None: 
        G = nx.DiGraph()
        l = str(node.question)+ "({0})({1})".format(i,addition)

        i += 1
        i,l1 = graph_tree(node.true_branch,i,graph=G,addition='T',m_depth=m_depth)
        i += 1
        i,l2 = graph_tree(node.false_branch,i,graph=G,addition='F',m_depth=m_depth)
        G.add_edge(l,l1)
        G.add_edge(l,l2)
        pos = graphviz_layout(G,prog='dot')
        nx.draw(G,pos,font_size=2,with_labels=False,edge_color='black',width=1,linewidths=1,node_size=10,node_color='pink',alpha=0.9)
        nx.draw_networkx_labels(G,pos,font_size=9)
        plt.show()
    # leaf node
    elif isinstance(node,Leaf_Node):
        return i+1,str(node.predictions) + "({0})({1})".format(i,addition)
    elif depth >= m_depth:
        return i+1,str(node.question) + "({0})({1})".format(i,addition)

    #node
    else:
        l = str(node.question) + "({0})({1})".format(i,addition)
        G = graph
        i+= 1
        i,l1 = graph_tree(node.true_branch,i,graph=G,depth=depth+1,m_depth=m_depth,addition='T')
        i+= 1
        i,l2 = graph_tree(node.false_branch,i,graph=G,depth=depth+1,m_depth=m_depth,addition='F')
        G.add_edge(l,l1)
        G.add_edge(l,l2)
        return i,l
    



def classify(x,node):
    if isinstance(node,Leaf_Node):
        curr_c = None
        curr_v = -float('inf')
        for val, item in node.predictions.items():
            if val > curr_v:
                curr_c = item
                curr_c = val
        return (curr_c,node.predictions)

    if  node.question.match(x):
        return classify(x,node.true_branch)
    return classify(x,node.false_branch)


class DecisionTree():
    def __init__(self):
        pass
    def train(self,X):
        self.root = build_tree(X)
    def classify(self,x):
        return classify(x,self.root)
    def print(self,max_depth=float('inf')):
        print_tree(self.root,depth=0,max_depth=max_depth)

    def graph(self,m_depth=4):
        graph_tree(self.root,0,m_depth=m_depth)




if __name__ == "__main__" :
    g = Generator(classes=4,dim=10, samples_per_class=500)
    i = g.generate()
    d = DecisionTree()
    d.train(i)

    print(i)
    d.print(max_depth=4)

    s = 0
    for x in i:
        l = d.classify(x)
        if l[0] == int(x[i.shape[1]-1]):
            s += 1.0

    print("Decision Tree classification")
    print(s/len(i))

    d.graph()

    g = Generator(classes=4,dim=10, samples_per_class=4)
    i = g.generate()
    d.train(i)
    d.graph(m_depth=float('inf'))


