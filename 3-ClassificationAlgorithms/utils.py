# TODO list
# 1.2.1 # Done -> dependence_tree, Generate

import random
import numpy as np
import math
from pprint import pprint

from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree

class dependence_tree():
    def __init__(self,root=None,children=None,feature=None):
        # root node
        self.root = root

        self.children = children
        #feature index
        self.feature = feature

        # class probility w_i

        p1 = random.random()
        p2 = random.random()
        p3 = random.random()


        self.class_p0 = p3
        self.class_p1 = 1.0-p3
        # probility of this node being 0 given root being 0
        self.pxi0_pxr0 = p1
        # probility of this node being 1 given root being 0
        self.pxi1_pxr0 = 1.0-p1
        # probility of this node being 0 given root being 1
        self.pxi0_pxr1 = p2
        # probility of this node being 1 given root being 1
        self.pxi1_pxr1 = 1.0-p2

    def __str__(self):
        return self.print_tree(self)

    def train(self,features): 
        # root node
        if self.root is None:
            self.class_p0 = self._p_x(features,0,self.feature)
            self.class_p1 = self._p_x(features,1,self.feature)
        else:
            self.pxi0_pxr0 = self._p_x_given_y(features,0,0,self.feature,self.root.feature)
            self.pxi1_pxr0 = self._p_x_given_y(features,1,0,self.feature,self.root.feature)
            self.pxi0_pxr1 = self._p_x_given_y(features,0,1,self.feature,self.root.feature)
            self.pxi1_pxr1 = self._p_x_given_y(features,1,1,self.feature,self.root.feature)

        for child in self.children:
            if not child is None:
                child.train(features)

    def _p_x_given_y(self,features,non_given,given,non_given_i,given_i):
        s = 0.0
        total = 0.0
        for item in features:
            if item[given_i] == given:
                total += 1.0
                if item[non_given_i] == non_given:
                    s += 1.0
        if total == 0:
            return 0.0
        return s/total

    def _p_x(self,features,non_given,non_given_i):
        s = 0.0
        total = 0.0
        for item in features:
            if item[non_given_i] == non_given:
                s += 1.0
            total += 1.0
        if total == 0:
            return 0.0
        return s/total

    def print_tree(self,node,spacing=""):
        f = node.feature
        r = "None"
        if not node.root == None:
            r = node.root.feature
        s = spacing+"#feature {0}, root feature -> {1}\n".format(f,r)

        if not node.root == None:
            s += spacing+"|    P(x{0}=0|x{1}=0)={2}\n".format(f,r,round(node.pxi0_pxr0,2))
            s += spacing+"|    P(x{0}=1|x{1}=0)={2}\n".format(f,r,round(node.pxi1_pxr0,2))
            s += spacing+"|    P(x{0}=0|x{1}=1)={2}\n".format(f,r,round(node.pxi0_pxr1,2))
            s += spacing+"|    P(x{0}=1|x{1}=1)={2}\n".format(f,r,round(node.pxi1_pxr1,2))
        else:
            s += spacing+"|   P(x{0}=0)={1}\n".format(f,round(node.class_p0,2))
            s += spacing+"|   P(x{0}=1)={1}\n".format(f,round(node.class_p1,2))

        if node.children == None:
            return s

        for child in node.children:
            s += self.print_tree(child,spacing=spacing+"|    ")
        return s

    def generate(self,dim=None):
        if dim is None :
            raise Exception("invalid value : 'dim' cannot be None")
        
        return self.__generate(np.zeros(dim))

    def __generate(self,x):
        r = random.random()
        if self.root == None:
            if self.class_p0 > r:
                x[self.feature] = 0
            else:
                x[self.feature] = 1
        else:
            if x[self.root.feature] == 0:
                if self.pxi0_pxr0 > r:
                    x[self.feature] = 0
                else:
                    x[self.feature] = 1
            else:
                if self.pxi0_pxr1 > r:
                    x[self.feature] = 0
                else:
                    x[self.feature] = 1
        
        if self.children is None:
            return x

        for child in self.children:
            x = child.__generate(x)

        return x
            

        
    

    def create(feature_format):
        if isinstance(feature_format,int):
            return dependence_tree(feature=feature_format)

        trees = [dependence_tree.create(item) for item in feature_format]

        root = trees[0]
        children = trees[1:]
        
        for child in children:
            child.root = root

        root.children = children

        return root


def create_model(features):
    print("Creating Model : ")

    # get classes and create graph
    f_index = list(range(features.shape[1]-1))
    classes = list(set(features[::,features.shape[1]-1].astype('int')))
    graph = np.zeros([len(f_index),len(f_index)])

    # seperate the training data
    indicies = [[] for i in classes]
    for i in range(len(features)):
        indicies[int(features[i,features.shape[1]-1])].append(i)
    training = [np.take(features,index,axis=0) for index in indicies]


    # calculate entropy
    for i in f_index:
        for j in f_index:
            if i <= j :
                continue
            graph[i,j] = graph[i,j] - entropy(training[0][::,i],training[0][::,j],0,0)
            graph[i,j] = graph[i,j] - entropy(training[0][::,i],training[0][::,j],0,1)
            graph[i,j] = graph[i,j] - entropy(training[0][::,i],training[0][::,j],1,0)
            graph[i,j] = graph[i,j] - entropy(training[0][::,i],training[0][::,j],1,1)

    # find the dependency graph
    X = csr_matrix(graph)
    Tcsr = minimum_spanning_tree(X)
    graph_m = Tcsr.toarray().astype(float)


    # convert to ajacency list
    adjacency_list = {}
    for i in f_index:
        for j in f_index:
            if not graph_m[i,j] == 0.0:
                if adjacency_list.get(i,None) is None:
                    adjacency_list[i] = set([j])
                else:
                    adjacency_list[i].add(j)

                if adjacency_list.get(j,None) is None:
                    adjacency_list[j] = set([i])
                else:
                    adjacency_list[j].add(i)

    # find the best dependency tree
    tree, _ = depth_first_search(adjacency_list,set(),[0])

    trees = [dependence_tree.create(tree) for i in range(len(classes))]

    for item, data in zip(trees,training):
        item.train(data)

    return trees, classes


def depth_first_search(adjacency_list,visited,node_list):

    current = node_list.pop(0)

    temp = [current]
    visited_r = None

    visited.add(current)

    for child in adjacency_list[current]:
        if child in visited:
            continue

        node_list.append(child)
        visited.add(child)

        tree, visited_r = depth_first_search(adjacency_list,visited,node_list)

        visited = visited_r
        temp.append(tree)

    return temp, visited

def entropy(f1,f2,vi,vj):
    s_vi    = 0.0
    s_vj    = 0.0
    s_vi_vj = 0.0
    total = float(len(f1))

    for i, j in zip(f1,f2):
        if i==vi and j == vj:
            s_vi_vj += 1.0
        if i==vi:
            s_vi += 1.0
        if j== vj:
            s_vj += 1.0

    if s_vi_vj == 0:
        return 0.0
    return (s_vi_vj/total)*math.log((s_vi_vj/total)/((s_vi/total)*(s_vj/total)),2)

class Generator(): 
    def __init__(self,classes=4,dim=10,samples_per_class=500,seed=0,tree_format=[0,[1,[2,3]],4,5,[6,7,8],9]):
        self.classes = classes
        self.dim = dim
        self.samples_per_class = samples_per_class
        self.tree_format = tree_format
        self.seed =seed

    def generate(self):
        samples = []
        random.seed(self.seed)
        # diffrent classes dependence trees
        w = [dependence_tree.create(self.tree_format) for i in range(self.classes)]

        print("Generator Genereating Trees")
        for i,wi in enumerate(w):
            print('CLASS : {0} -------------------'.format(i))
            print(wi)


        for i in range(self.classes):
            for j in range(self.samples_per_class):
                samples.append(  np.concatenate((w[i].generate(dim=self.dim),np.asarray([float(i)])),axis=None))

        return np.asarray(samples)



if __name__ == "__main__" :
    #g = Generator(dim=3,classes=10,samples_per_class=400)
    g = Generator(samples_per_class=1000)
    test = g.generate()
    #print(test)
    model = create_model(test)

    for item in model:
        print(item)

    
