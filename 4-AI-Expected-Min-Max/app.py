import random
import argparse
import pygame
import numpy as np
import heapq
from pygame.locals import *
from collections import deque
import copy
from pprint import pprint
import time
import json

DEBUG = False
DEBUG_AI = False


class Abstract_Heuristic:
    def __init__(self,model):
        self._model = model
    
    # dummy method for heuristic 
    def cal(self,state,player):
        return 1

class Loc_H(Abstract_Heuristic):
    def cal(self,state,player):
        s = 0
        for key, items in state.players_marbels.items():
            s1 = 0
            for item in items:
                if item is None:
                    pass
                elif item >= 0:
                    # state 71 is not needed
                    s1 += (item + 1 )% 72
                elif item < 0:
                    # getting in the win position is more important than any other move
                    s1 += 4*72
            # current player
            if key == player:
                s += s1
            else:
                s -= s1

        return s/(4*4*4*72)
        

# does nothing
class Abstract_Strategy:
    def __init__(self,model,player_num,heuristic=None,depth=2,name=""):
        self.model = model
        self.moves = None
        self.player = player_num
        self.heuristic = heuristic
        self.depth = depth
        self.name = name

        self.heat_map = np.zeros(100)
        self.nodes = []
        self.won = 0
        self.n = 0

    def get(self):
        pass

    def new_game(self,winner):
        self.n += 1

        if winner == self.player:
            self.won += 1

    def stats(self):
        m = {}
        m['name'] = self.name
        m['mean'] = np.mean(self.nodes)
        m['var']  = np.var(self.nodes)
        m['std']  = np.std(self.nodes)
        m['n']    = self.n
        m['won']  = self.won
        #m['nodes'] = self.nodes
        #m['heat'] = self.heat_map
        
        
        return  m

    def random_moves(self,state,player,against):
        # only generate for player
        s = state.copy()
        played = s.played_cards 
        possible_cards = []
        children = []
        
        # make sure that opponents have no cards for production
        for p in s.player_order:
            s.player_hands[p] = []

        # cards that can be played
        for card in range(1,14):
            if played.count(card) <= 4:
                possible_cards.append(card)

        # we whent over the deck limit, and we have no shuffling
        if len(s.deck) > 52:
            s.deck = []

        # generate for player
        if not against:
            for card in possible_cards:
                s.player_hands[player] = [card]
                kids = self.model.production(player,s,shuffle=False)

                p = (4.0 - float(played.count(card)))/52.0
                for kid in kids:
                    children.append((p,kid))
        # only generate for not player
        else:
            for antagonist in s.player_order:
                if not antagonist == player:
                    for card in possible_cards:
                        s.player_hands[antagonist] = [card]
                        kids = self.model.production(antagonist,s,shuffle=False)

                        p = (4.0 - float(played.count(card)))/52.0
                        for kid in kids:
                            children.append((p,kid))
        
        # k is some kind of normalizing constant for the probilities, since all children have the same chance of being picked
        k = sum([i for i, j in children])
        if k > 0:
            ret = [(p/k, kid) for p, kid in children]
        else:
            ret = children
        return ret


class Monte_Carlo(Abstract_Strategy):
    def get(self):
        self.nodes.append(0)
        moves = self.model.production(self.player,self.model.state)
        return random.choice(moves)

class BestReplyExpectiMinMax(Abstract_Strategy):
    def get(self):
        self.nodes.append(0)
        val , move_info = self.expecti_mini_max(self.model.state,self.depth,True,self.player)
        print("BestReplyExpectiMiniMax -- player : {}, val : {}, move : {}, card : {}".format(self.player,val,move_info[1],move_info[2]))
        return move_info

    def expecti_mini_max(self,node,depth,maximize_player,player):
        self.nodes[len(self.nodes)-1] += 1
        children = []
        t_s = None
        val = None


        if self.model.win(node) or depth <= 0:
            return (self.heuristic.cal(node,player),node)


        if maximize_player: 
            # random children
            if len(node.player_hands[player]) <= 0:
                val = 0
                t_s = (node,None,None)
                children = self.random_moves(node,player,False)
                for p, (state,move,card) in children:
                    (v,s) = self.expecti_mini_max(state,depth-1,False,player)
                    val += p*v
                # random children
            else:
                val = -float('inf')
                children = self.model.production(player,node,shuffle=False)
                for state, move, card in children:
                    (v,s) = self.expecti_mini_max(state,depth-1,False,player)
                    if v > val:
                        val = v
                        t_s = (state,move,card)

        # always random
        else:
            val = 0 
            t_s = (node,None,None)
            children = self.random_moves(node,player,True)

            for p, (state,move,card) in children:
                (v,s) = self.expecti_mini_max(state,depth-1,False,player)
                val += p*v
        return (val,t_s) 

class BestReplyMinMaxAlphaBeta(Abstract_Strategy):
    def get(self):
        self.nodes.append(0)

        val , move_info = self.expecti_mini_max(self.model.state,self.depth,True,self.player,-float('inf'),float('inf'))
        print("BestReplyMinMaxAlphaBeta -- player : {}, val : {}, move : {}, card : {}".format(self.player,val,move_info[1],move_info[2]))
        return move_info

    def expecti_mini_max(self,node,depth,maximize_player,player,a,b):
        self.nodes[len(self.nodes)-1] += 1

        children = []
        t_s = None
        val = None


        if self.model.win(node) or depth <= 0:
            return (self.heuristic.cal(node,player),node)


        if maximize_player: 
            # random children
            if len(node.player_hands[player]) <= 0:
                val = -float('inf')
                children = self.random_moves(node,player,False)
                random.shuffle(children)
                for p, (state,move,card) in children:
                    (v,s) = self.expecti_mini_max(state,depth-1,False,player,a,b)
                    # check if there is a better value
                    if v > val:
                        val = v
                        t_s = (state,move,card)
                    # check if this value beats alpha
                    if val > a:
                        a = val
                    # check if there is a shorter path
                    if a >= b:
                        break
            else:
                val = -float('inf')
                children = self.model.production(player,node,shuffle=False)
                random.shuffle(children)
                for state, move, card in children:
                    (v,s) = self.expecti_mini_max(state,depth-1,False,player,a,b)
                    # check if there is a better value
                    if v > val:
                        val = v
                        t_s = (state,move,card)
                    # check if this value beats alpha
                    if val > a:
                        a = val
                    # check if there is a shorter path
                    if a >= b:
                        break

        # always random
        else:
            val = float('inf')
            t_s = (node,None,None)
            children = self.random_moves(node,player,True)
            random.shuffle(children)

            for p, (state,move,card) in children:
                (v,s) = self.expecti_mini_max(state,depth-1,True,player,a,b)
                # check if there is a better value
                if v < val:
                    val = v
                    t_s = (state,move,card)
                # check if this value beats alpha
                if val < b:
                    b = val
                # check if there is a shorter path
                if a >= b:
                    break
        return (val,t_s) 

class GameState():
    def __init__(self):
        self.reset()

    def custom(self,deck,played_cards,players_marbels,player_order,player_hands):
        #[,,,,,]
        self.deck = deck
        self.played_cards = played_cards

        #{i:[[],j:[],k:[],l:[]}
        # list of lists, where first list is player i's marbels
        self.players_marbels = players_marbels

        # [i,j,k,l]
        # first to last
        self.player_order = player_order

        #{i:[[],j:[],k:[],l:[]}
        # list of lists, where first list is player i's cards
        self.player_hands = player_hands

        # dealer id
        self.dealer = 0

        return self

    def reset(self):
        self.deck = np.repeat(np.arange(13)+1,4).tolist()
        random.shuffle(self.deck)

        self.played_cards = []
        self.players_marbels = {}
        self.player_hands = {}
        self.player_order = list(range(4))

        for player in self.player_order:
            self.players_marbels[player] = [None,None,None,None]
            self.player_hands[player] = self.deck[0:5]
            self.deck = self.deck[5:]

        # dealer id
        self.dealer = 0


    def copy(self):
        g = GameState()
        a = copy.deepcopy(self.deck)
        b = copy.deepcopy(self.played_cards)
        c = copy.deepcopy(self.players_marbels)
        d = copy.deepcopy(self.player_order)
        e = copy.deepcopy(self.player_hands)

        return g.custom(a,b,c,d,e)

    def __repr__(self):
        s = "\n State :\n Players : {0} \n Deck : {1} \n Player Order : {2} \n Player_Hands : {3}\n Marble Pos : {4}\n".format(self.player_order,self.deck,self.player_order,self.player_hands,self.players_marbels)
        return s





class Model:
    def __init__(self,args,options_1_m):
        print("Creating Model")
        p1 = None
        p2 = None
        p3 = None
        p4 = None

        if not args.p1 == 'player':
            p1 = options_1_m[args.p1](self,0,heuristic=Loc_H(self),depth = args.d1,name=args.p1)
        if not args.p2 == 'player':
            p2 = options_1_m[args.p2](self,1,heuristic=Loc_H(self),depth = args.d2,name=args.p2)
        if not args.p3 == 'player':
            p3 = options_1_m[args.p3](self,2,heuristic=Loc_H(self),depth = args.d3,name=args.p3)
        if not args.p4 == 'player':
            p4 = options_1_m[args.p4](self,3,heuristic=Loc_H(self),depth = args.d4,name=args.p4)

        players = [p1,p2,p3,p4]
        is_ai = [not p == None for p in players]
        print(is_ai)
        print(args)

        self.init(is_ai,players)


            

        #self.init([True,True,True,True],[Monte_Carlo(self,0),Monte_Carlo(self,1),Monte_Carlo(self,2),Monte_Carlo(self,3)])
        '''
        self.init([True,True,True,True],
                [BestReplyExpectiMinMax(self,0,heuristic=Loc_H(self)),
                    BestReplyExpectiMinMax(self,1,heuristic=Loc_H(self)),
                    BestReplyExpectiMinMax(self,2,heuristic=Loc_H(self)),
                    BestReplyExpectiMinMax(self,3,heuristic=Loc_H(self))])
        self.init([True,True,True,True],
                [BestReplyExpectiMinMax(self,0,heuristic=Loc_H(self)),
                    BestReplyMinMaxAlphaBeta(self,1,heuristic=Loc_H(self)),
                    BestReplyExpectiMinMax(self,2,heuristic=Loc_H(self)),
                    BestReplyMinMaxAlphaBeta(self,3,heuristic=Loc_H(self))])
                    '''
        #self.init([False,False,False,False],[Monte_Carlo(self,0),Monte_Carlo(self,1),Monte_Carlo(self,2),Monte_Carlo(self,3)])
    def new_game(self):
        winner = self.win(self.state,number=True)
        for ai in self.strategys:
            ai.new_game(winner)
        self.state = GameState()

    def init(self,ai_players,strategys):
        print("Re-initializing Model")
        self.ai_players = ai_players
        self.strategys = strategys
        self.state = GameState()

        # remove
        #self.state = self.production(0,self.state)[0][0]
        print(self.state)
        '''
        self.state.players_marbels[0] = [0,None,None,None]
        self.state.players_marbels[1] = [0,None,None,None]
        self.state.player_hands[0] = [11,1,1,1]
        self.state.player_hands[1] = [11,1,1,1]
        self.state.player_hands[2] = [11,1,1,1]
        self.state.player_hands[3] = [11,1,1,1]
        '''


    # returns the board size currently being played on
    def get_board_size(self):
        return self._board_size

    # gets the next move defined by the srategy
    def player_move(self,marble,pos):
        if self.win(self.state):
            print("MODEL -- WIN")
            return 
        if self.ai_players[self.state.player_order[0]]:
            print("MODEL -- INVALID MOVE -- AI TURN")
            return
        player = self.state.player_order[0]

        print('MODEL - Player : {}, Marble : {}, Pos : {}'.format(player,marble,pos))

        products = self.production(player,self.state)

        states = [product[0] for product in products]
        moves = [product[1] for product in products]
        cards = [product[2] for product in products]

        if (None,None) in moves:
            self.state = products[0][0]
        elif not (marble,pos) in moves:
            print("invalid move")
        else:
            for state, move in zip(states,moves):
                if move == (marble,pos):
                    self.state = state
                    break
        if DEBUG:
            print('Possible Moves')
            print(moves)
            print(cards)
            print(Loc_H(self).cal(self.state,player))

            print(self.state)
        
            
        return


    def ai_move(self):
        state = None
        move = None
        card = None

        if DEBUG_AI:
            print("AI_MOVE -- player {}".format(self.state.player_order[0]))
        if self.win(self.state):
            print("win")
            return
        if not self.ai_players[self.state.player_order[0]]:
            if DEBUG_AI:
                print("MODEL -- INVALID MOVE -- PLAYER TURN")
            return

        products = self.production(self.state.player_order[0],self.state)

        states = [product[0] for product in products]
        moves = [product[1] for product in products]
        cards = [product[2] for product in products]

        if (None,None) in moves:
            state = products[0][0]
        else:
            state, move, card = self.strategys[self.state.player_order[0]].get()

            if move in moves:
                if DEBUG_AI:
                    print("move : {}, card : {} ".format(move,card))
            else:
                print("INVALID MOVE")
                raise Exception

        self.state = state
        if DEBUG_AI:
            print(self.state)


    def win(self,state,number=False):
        for key, items in state.players_marbels.items():
            t = True
            for item in items:
                if item is None:
                   t = False 
                elif item < 0:
                    t = t and True
                else:
                    t = False
            if t and not number :
                return True
            elif t and number:
                return key
            
        if not number:
            return False
        return None
                    

    def production(self,player,state,shuffle=True):
        # if one wanted to skip a player
        player_list = state.player_order
        player_index = state.player_order.index(player)

        a = state.player_order[0:player_index+1]
        b = state.player_order[player_index+1:]
        player_list = b+a


        moves = []

        for card in state.player_hands[player]:
            for c, marble_pos in enumerate(state.players_marbels[player]):
                # marble can move in its own zone
                if not marble_pos is None and marble_pos < 0 and marble_pos - card >= -4 and not marble_pos-card in state.players_marbels[player]:
                    # new state
                    s = state.copy()
                    s.deck
                    s.played_cards.append(card)
                    s.players_marbels[player][c] = marble_pos-card
                    s.player_hands[player].remove(card)
                    s.player_order = player_list

                    moves.append((s,(c,s.players_marbels[player][c]),card))
                # marble can move into its zone
                if not marble_pos is None and marble_pos >= 0 and -(marble_pos + card) + 70 >= -4 and -(marble_pos + card) + 70 < 0 and not -(marble_pos + card) + 70 in state.players_marbels[player]:
                    # new state
                    s = state.copy()
                    s.deck
                    s.played_cards.append(card)
                    s.players_marbels[player][c] = -(marble_pos + card) + 70
                    s.player_hands[player].remove(card)
                    s.player_order = player_list

                    moves.append((s,(c,s.players_marbels[player][c]),card))
                # regualar turn
                if not marble_pos is None and marble_pos >= 0 and not (marble_pos + card) % 72 in state.players_marbels[player]:
                    # new state
                    s = state.copy()
                    s.deck
                    s.played_cards.append(card)
                    s.players_marbels[player][c] = (marble_pos + card) % 72
                    s.player_hands[player].remove(card)
                    s.player_order = player_list

                    s.players_marbels = self.kill_if_landed(s.players_marbels,player)

                    moves.append((s,(c,s.players_marbels[player][c]),card))
                # getting out ace or king
                if (card == 1 or card == 13) and marble_pos is None and not card-1 in state.players_marbels[player]:
                    # new state
                    s = state.copy()
                    s.deck
                    s.played_cards.append(card)
                    s.players_marbels[player][c] = 0
                    s.player_hands[player].remove(card)
                    s.player_order = player_list

                    s.players_marbels = self.kill_if_landed(s.players_marbels,player)

                    moves.append((s,(c,s.players_marbels[player][c]),card))
                # jack -- do not implement right now
                if not marble_pos is None and (card == 11) and marble_pos >= 0:
                    # new states
                    
                    for p in state.player_order:
                        if not p == player:
                            for counter,other_pos in enumerate(state.players_marbels[p]):
                                if not other_pos is None and other_pos >= 0:
                                    s = state.copy()
                                    s.deck
                                    s.played_cards.append(card)
                                    s.player_hands[player].remove(card)

                                    temp = s.players_marbels[player][c]


                                    # need to convert these to each other
                                    l1 = (((s.players_marbels[player][c]+ 18*player)%72) + 72 - 18*p)%72
                                    l2 = (((s.players_marbels[p][counter]+ 18*p)%72) + 72 - 18*player)%72

                                    s.players_marbels[player][c] = l2
                                    s.players_marbels[p][counter] = l1

                                    s.player_order = player_list

                                    moves.append((s,(c,s.players_marbels[player][c]),card))





        if len(moves) <= 0:
            s = state.copy()
            s.deck
            s.played_cards = s.played_cards + s.player_hands[player]
            s.players_marbels
            s.player_hands[player] = []
            s.player_order = player_list
            moves.append((s,(None,None),None))

        if shuffle:
            temp = []
            for state, move, card in moves:
                a = self.shuffle(state)
                temp.append((a,move,card))
            moves = temp


        return moves

    def shuffle(self,state,player=None):
        if DEBUG:
            print("SHUFFLE ---- ")
        s = state.copy()
        cards = []
        for i,j in s.player_hands.items():
            cards = cards + j
        #cards = [ j for i,j in s.player_hands.items()]


        if len(cards) <= 0 and len(s.deck) <= 0:
            if DEBUG:
                print("Shuffle : new deck")

            # change the dealer
            s.dealer += 1
            if s.dealer > max(s.player_order):
                s.dealer = 0

            if DEBUG:
                print("SHUFFLE : Dealer {}".format(s.dealer))

            # person who starts is after the dealer
            player_index = s.player_order.index(s.dealer)

            a = s.player_order[0:player_index+1]
            b = s.player_order[player_index+1:]
            player_list = b+a

            s.player_order = player_list

            s.deck = s.played_cards
            s.played_cards = []

            random.shuffle(s.deck)

            for player in s.player_order:
                s.player_hands[player] = s.deck[0:5]
                s.deck = s.deck[5:]

        elif len(cards) <= 0 and len(s.deck) >= 0:
            if DEBUG:
                print('Shuffle : Hand out cards')

            for player in s.player_order:
                s.player_hands[player] = s.deck[0:4]
                s.deck = s.deck[4:]
        return s
            

    def kill_if_landed(self,marbles,player):
        transform = {}
        transform = copy.deepcopy(marbles)
        for key, items in transform.items():
            for c, item in enumerate(items):
                if item is None:
                    pass
                elif item < 0:
                    pass
                elif item >= 0:
                    transform[key][c] = (item + 18*key)%72

        for item_1 in transform[player]:
            for key, items in transform.items():
                if key == player:
                    continue


                for c, item in enumerate(items):
                    if item is None:
                        pass
                    elif item < 0:
                        pass
                    elif item >= 0 and item == item_1:
                        transform[key][c] = None

        for key, items in transform.items():
            for c, item in enumerate(items):
                if item is None:
                    pass
                elif item < 0:
                    pass
                elif item >= 0:
                    transform[key][c] = (72 + item - 18*key)%72

        return transform

        

                    
    def move(self,state,player,marble,card,pos):
        pass
        # what is needed for a new state 
        # - kill other marbles that I land on
            # - reset other marble
        # - change the current marbel pos to its new marble pos
        # - fix player list order
        # - remove card that was played


class Controller:
    def __init__(self,args,ops):
        self.model= Model(args,ops)
        print(self.model.state)
        s = self.model.state.copy()

        b = np.zeros((19,19))

        #positions 
        b1 = [(i+1,0) for i in range(b.shape[1]-1)]
        b2 = [(b.shape[1]-1,i+1)for i in range(b.shape[1]-1)]
        b3 = [(i,b.shape[1]-1) for i in range(b.shape[1]-1)][::-1]
        b4 = [(0,i) for i in range(b.shape[1]-1)][::-1]

        # end zone marble positions
        p1 = [(i+1,i+1) for i in range(4)]
        p2 = [(b.shape[1]-2-i,i+1)for i in range(4)]
        p3 = [(b.shape[1]-2-i,b.shape[1]-2 -i)for i in range(4)]
        p4 = [(i+1,b.shape[1]-2-i) for i in range(4)]

        # mapping for end marbles
        p1_m = {j:i for i,j in zip(p1,list(range(-4,0))[::-1])}
        p2_m = {j:i for i,j in zip(p2,list(range(-4,0))[::-1])}
        p3_m = {j:i for i,j in zip(p3,list(range(-4,0))[::-1])}
        p4_m = {j:i for i,j in zip(p4,list(range(-4,0))[::-1])}

        # mapping of posisions 
        b_t = b1+b2+b3+b4

        # model to render
        b_mr = {i:j for i,j in zip(range(len(b_t)),b_t)}

        # render to model
        b_rm = {j:i for i,j in zip(range(len(b_t)),b_t)}
        
        for key, item in p1_m.items():
            b_rm[item] = key
        for key, item in p2_m.items():
            b_rm[item] = key
        for key, item in p3_m.items():
            b_rm[item] = key
        for key, item in p4_m.items():
            b_rm[item] = key


        self.pos_to_board_pos = b_rm



    def submit_move(self,marble_id,pos):
        pos_m = self.pos_to_board_pos.get(pos,None)
        i = self.model.state.copy().player_order[0]

        if pos_m >= 0:
            pos_m = (72 + pos_m - 18*i)%72

        if pos_m is None:
            print("invalid pos")
            return 

        print('CONTROLLER - player : {}, marble : {}, pos_m : {}, pos :{}'.format(i,marble_id,pos_m,pos))

        self.model.player_move(marble_id,pos_m)
        # convert board position to actual position
        # send to model, model checks
            # check if move is in the production system
            # submits the move if correct
        pass

    def get_board(self):
        s = self.model.state.copy()

        b = np.zeros((19,19))

        #positions 
        b1 = [(i+1,0) for i in range(b.shape[1]-1)]
        b2 = [(b.shape[1]-1,i+1)for i in range(b.shape[1]-1)]
        b3 = [(i,b.shape[1]-1) for i in range(b.shape[1]-1)][::-1]
        b4 = [(0,i) for i in range(b.shape[1]-1)][::-1]

        # drawing positsions
        for i in range(b.shape[1]-1):
            b[b1[i]] = 1
            b[b2[i]] = 2
            b[b3[i]] = 3
            b[b4[i]] = 4


        # end zone marble positions
        p1 = [(i+1,i+1) for i in range(4)]
        p2 = [(b.shape[1]-2-i,i+1)for i in range(4)]
        p3 = [(b.shape[1]-2-i,b.shape[1]-2 -i)for i in range(4)]
        p4 = [(i+1,b.shape[1]-2-i) for i in range(4)]

        # mapping for end marbles
        p1_m = {j:i for i,j in zip(p1,list(range(-4,0))[::-1])}
        p2_m = {j:i for i,j in zip(p2,list(range(-4,0))[::-1])}
        p3_m = {j:i for i,j in zip(p3,list(range(-4,0))[::-1])}
        p4_m = {j:i for i,j in zip(p4,list(range(-4,0))[::-1])}

        pt_m = [p1_m,p2_m,p3_m,p4_m]

        # mapping of posisions 
        b_t = b1+b2+b3+b4
        b_m = {i:j for i,j in zip(range(len(b_t)),b_t)}

        for i in range(4):
            b[p1[i]] = 1
            b[p2[i]] = 2
            b[p3[i]] = 3
            b[p4[i]] = 4
        
        marbel_mapping = {i:[] for i in s.players_marbels.keys()}
        #print(s.players_marbels)
        # key is player id
        # items : marble positions
        for key, items in s.players_marbels.items():
            # c is marble marble id
            # item is local marble position
            for c,item in enumerate(items): 
                if item is None:
                    continue
                if item < 0:
                    #marbel_mapping[key] = 
                    marbel_mapping[key].append((c,pt_m[key][item]))
                    pass
                if item >= 0:
                    #print(key,items,c,item)
                    marbel_mapping[key].append((c,b_m[(item+key*18)%72]))

        

        return b,s.player_hands,marbel_mapping

    def winner(self):
        return self.model.win(self.model.state)

    def ai_move(self):
        self.model.ai_move()

    def new_game(self):
        self.model.new_game()

# templating code from : http://pygametutorials.wikidot.com/tutorials-two
class App:
    def __init__(self,args,ops,speed=1000):
        self.pos = 0
        self._running = True
        self._display_surf = None
        self._image_surf = None
        self._bg_colour = (0,0,0)
        self._board = None
        self._x_boarder = 800
        self._y_boarder = 800
        self.strategy = "Null"
        self.speed = speed

        self._controller = Controller(args,ops)

 
    def on_init(self):
        pygame.init()
        pygame.display.set_caption("4 Connect [{0}]".format(self.strategy))
        self._time = pygame.time.get_ticks()

        self._display_surf = pygame.display.set_mode((self._x_boarder,self._y_boarder), pygame.HWSURFACE)
        self._running = True


        # get this from controller class ( dummy test ) 
        self._board = self._controller.get_board()
 
    def on_event(self, event):
        if event.type == QUIT:
            self._running = False

    def on_loop(self):
        pass

    def on_render(self):
        self._board, self.cards,self.marbels = self._controller.get_board()
        self._display_surf.fill(self._bg_colour)

        x_step = self._x_boarder/self._board.shape[0]
        y_step = self._y_boarder/self._board.shape[1]

        m_grid = self.get_mouse_pos()

        ratio = 5

        font = pygame.font.Font('freesansbold.ttf', 30) 


        for row, x in zip(self._board,range(self._board.shape[0])):
            for val, y in zip(row,range(self._board.shape[1])):


                colour = (0,0,0)
                boarder = (255,255,255)
                margin  = 2

                #player 1
                if val == 1:
                    colour = (150,0,0)
                #player 2
                if val == 2:
                    colour = (0,150,0)
                #player 3
                if val == 3:
                    colour = (0,0,150)
                #player 4
                if val == 4:
                    colour = (150,150,0)


    



                if m_grid == (x,y):
                    c1 = (colour[0] + 255)/2
                    c2 = (colour[1] + 255)/2
                    c3 = (colour[2] + 255)/2
                    colour = (c1,c2,c3)


                pygame.draw.rect(self._display_surf,boarder,pygame.Rect((x_step*x,y_step*y,x_step,y_step)))
                pygame.draw.rect(self._display_surf,colour,pygame.Rect((x_step*x,y_step*y,x_step-margin,y_step-margin)))
                
                # possible move for player
                if val == 10:
                    pygame.draw.rect(self._display_surf,(255,255,255),pygame.Rect((x_step*x,y_step*y,(x_step-margin)/ratio,(y_step-margin)/ratio)))


        # 5,6
        for i,c in self.cards.items():
            for j in range(len(c)+1):
                if j == 0:
                    text = font.render('P{0}'.format(i), True, (255,255,255),(0,0,0)) 
                else:
                    text = font.render('{0}'.format(c[j-1]), True, (255,255,255),(0,0,0)) 

                textRect = text.get_rect()  
                textRect.topleft = (x_step*i+x_step*5,y_step*j+ y_step*6) 
                self._display_surf.blit(text, textRect) 


        #print(self.marbels)
        for player, marbels in self.marbels.items():
            for marbel_id, pos in marbels:

                colour = (0,0,0)
                #player 1 marble
                if player == 0:
                    colour = (255,0,0)
                #player 2 marble
                if player == 1:
                    colour = (0,255,0)
                #player 3 marble
                if player == 2:
                    colour = (0,0,255)
                #player 4 marble
                if player == 3:
                    colour = (255,255,0)

                pygame.draw.rect(self._display_surf,colour,pygame.Rect((x_step*pos[0],y_step*pos[1],x_step-margin,y_step-margin)))

                textRect = text.get_rect()  
                text = font.render('{0}'.format(marbel_id+1), True, (255,255,255),colour) 
                textRect.topleft = (x_step*pos[0],y_step*pos[1]) 
                self._display_surf.blit(text, textRect) 

            

        pygame.display.flip()

    def get_mouse_pos(self):
        pos     = pygame.mouse.get_pos()
        x_step = self._x_boarder/self._board.shape[0]
        y_step = self._y_boarder/self._board.shape[1]

        m_grid = (int((pos[0]/x_step)),int((pos[1]/y_step)))

        return m_grid


    def on_cleanup(self):
        pygame.quit()
 
    def on_execute(self):
        if self.on_init() == False:
            self._running = False
 
        pressed = False
        marble_id = 0
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)

            keys = pygame.key.get_pressed()
            #self.get_mouse_pos()
            
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    marble_id = 0
                if event.key == pygame.K_2:
                    marble_id = 1
                if event.key == pygame.K_3:
                    marble_id = 2
                if event.key == pygame.K_4:
                    marble_id = 3

            if pygame.mouse.get_pressed()[0] == True  and pressed == False:
                pressed = True

            if pygame.mouse.get_pressed()[0] == False and pressed == True:
                self._time = pygame.time.get_ticks()
                pressed = False
                pos = self.get_mouse_pos()
                if self._board[pos] > 0:
                    # submit move
                    self._controller.submit_move(marble_id,pos)

            if pygame.time.get_ticks() - self._time > self.speed:
                self._controller.ai_move()
                self._time = pygame.time.get_ticks()

            if self._controller.winner():
                self._controller.new_game()

            self.on_loop()
            self.on_render()
        self.on_cleanup()
 

if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description='---- 4 Connect ----')

    options_1 = ['player','random','expectiminimax','minmaxalphabeta']
    options_1_m = {'player':None,
                    'random':Monte_Carlo,
                    'expectiminimax':BestReplyExpectiMinMax,
                    'minmaxalphabeta':BestReplyMinMaxAlphaBeta}

    parser.add_argument('--p1', choices=options_1, help='Who is Player 1',default='player')
    parser.add_argument('--p2', choices=options_1, help='Who is Player 2',default='player')
    parser.add_argument('--p3', choices=options_1, help='Who is Player 3',default='player')
    parser.add_argument('--p4', choices=options_1, help='Who is Player 4',default='player')

    parser.add_argument('--d1',type=int, help='Depth of Search Player 1',default=2)
    parser.add_argument('--d2',type=int, help='Depth of Search Player 2',default=2)
    parser.add_argument('--d3',type=int, help='Depth of Search Player 3',default=2)
    parser.add_argument('--d4',type=int, help='Depth of Search Player 4',default=2)



    

    parser.add_argument('--n',type=int, help='number of games to simulate',default=100)
    parser.add_argument('--output',type=str, help='output file for data',default='output.json')
    parser.add_argument('--speed',type=int, help='frames period time (ms)',default=20)
    parser.add_argument('--gui',choices=['t','f'], help='use gui',default='t')


    args = parser.parse_args()
    
    if args.gui == 't':
        app = App(args,options_1_m,speed=args.speed)
        app.on_execute()
    else:
        print("headless-mode")
        print("number of simulations : ",args.n)
        n = 0
        model = Model(args,options_1_m)
        while n < args.n:
            if model.win(model.state):
                n += 1
                model.new_game()

                data = []
                for ai in model.strategys:
                    data.append(ai.stats())

                pprint(data)
                results = json.dumps(data,sort_keys=True,indent=4,separators=(',',': '))
                with open(args.output,'w') as f:
                    f.write(results)
            model.ai_move()


